# Work @ Dextra

Esse documento é o passo-a-passo que é parte dos artefatos dos entregáveis do processo seletivo da Dextra.
Os passos abaixo são para ser executados em um ambiente linux debian/ubuntu based. Os dois primeiros passos são para configuração inicial do ambiente, sendo opcionais.

### Passo-a-Passo

```
$ [sudo] apt-get install python-pip python-dev build-essential
$ [sudo] pip install virtualenv
$ virtualenv --python=python3.6 venv
$ source venv/bin/activate
$ pip install -r requirements.txt
$ cd my_project
$ python3.6 manage.py test
$ python3.6 manage.py runserver
```

Este readme.md foi editado com Dillinger.


[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)


   [dill]: <https://github.com/joemccann/dillinger>
      [git-repo-url]: <https://github.com/joemccann/dillinger.git>
         [john gruber]: <http://daringfireball.net>
            [df1]: <http://daringfireball.net/projects/markdown/>
               [markdown-it]: <https://github.com/markdown-it/markdown-it>
                  [Ace Editor]: <http://ace.ajax.org>
                     [node.js]: <http://nodejs.org>
                        [Twitter Bootstrap]: <http://twitter.github.com/bootstrap/>
                           [jQuery]: <http://jquery.com>
                              [@tjholowaychuk]: <http://twitter.com/tjholowaychuk>
                                 [express]: <http://expressjs.com>
                                    [AngularJS]: <http://angularjs.org>
                                       [Gulp]: <http://gulpjs.com>
                                          [PlDb]: <https://github.com/joemccann/dillinger/tree/master/plugins/dropbox/README.md>
                                             [PlGh]: <https://github.com/joemccann/dillinger/tree/master/plugins/github/README.md>
                                                [PlGd]: <https://github.com/joemccann/dillinger/tree/master/plugins/googledrive/README.md>
                                                   [PlOd]: <https://github.com/joemccann/dillinger/tree/master/plugins/onedrive/README.md>
                                                      [PlMe]: <https://github.com/joemccann/dillinger/tree/master/plugins/medium/README.md>
                                                         [PlGa]: <https://github.com/RahulHP/dillinger/blob/master/plugins/googleanalytics/README.md>
                                                         `
