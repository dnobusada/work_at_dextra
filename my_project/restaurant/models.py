from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from django.urls import reverse

# Create your models here.


class Ingredient(models.Model):
    name = models.TextField(
        max_length=50,
        blank=False,
        help_text='De um nome ao ingrediente'
    )
    price = models.FloatField(
        validators=[MinValueValidator(0.01), MaxValueValidator(100)],
        help_text='De um preco do ingrediente'
    )

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('ingredient-detail', args=[str(self.id)])


class Dish(models.Model):
    name = models.TextField(
        max_length=50,
        blank=False,
        null=False,
        help_text='De um nome ao lanche'
    )
    ingredients = models.ManyToManyField(
        Ingredient, help_text='Adicione ingredientes ao lanche',
        through='DishIngredient'
    )

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('name',)

    def calculates_price(self):
        ingredient_list = list(DishIngredient.objects.filter(dish_id=self.id).all())
        meat = Ingredient.objects.filter(name='Hamburguer de carne')[0]
        cheese = Ingredient.objects.filter(name='Queijo')[0]
        meat_discount = self.much_meat(ingredient_list)
        cheese_discount = self.much_cheese(ingredient_list)
        total = 0

        for ingredient in ingredient_list:
            total += ingredient.ingredient.price * ingredient.amount_of_ingredients

        if meat_discount > 0:
            total -= meat_discount * meat.price

        if cheese_discount > 0:
            total -= cheese_discount * cheese.price

        if self.is_light(ingredient_list):
            total *= 0.9

        return total

    def get_ingredients_list_name(self):
        ingredient_list = list(DishIngredient.objects.filter(dish_id=self.id))
        ingredient_list_name = [ingr.ingredient.name for ingr in ingredient_list]
        return ingredient_list_name

    def get_absolute_url(self):
        return reverse('dish-detail', args=[str(self.id)])

    def is_light(self, dish_ingredients):
        bacon = Ingredient.objects.filter(name='Bacon')[0]
        lettuce = Ingredient.objects.filter(name='Alface')[0]
        bacon_validation = bacon not in [di.ingredient for di in dish_ingredients]
        lettuce_validation = lettuce in [di.ingredient for di in dish_ingredients]
        if bacon_validation and lettuce_validation:
            return True
        return False

    def much_meat(self, dish_ingredients):
        hamburger = Ingredient.objects.filter(name='Hamburguer de carne').all()[0]
        if hamburger not in [di.ingredient for di in dish_ingredients]:
            return 0
        for ingr in dish_ingredients:
            if ingr.ingredient == hamburger and ingr.amount_of_ingredients > 2:
                return ingr.amount_of_ingredients // 3
            else:
                return 0

    def much_cheese(self, dish_ingredients):
        cheese = Ingredient.objects.filter(name='Queijo').all()[0]
        if cheese not in [di.ingredient for di in dish_ingredients]:
            return 0
        for ingr in dish_ingredients:
            if ingr.ingredient == cheese and ingr.amount_of_ingredients > 2:
                return ingr.amount_of_ingredients // 3
            else:
                return 0


class DishIngredient(models.Model):
    dish = models.ForeignKey(
        Dish,
        on_delete=models.CASCADE
    )
    ingredient = models.ForeignKey(
        Ingredient,
        on_delete=models.CASCADE
    )
    amount_of_ingredients = models.IntegerField(
        default=1,
        null=False,
        validators=[MinValueValidator(0), MaxValueValidator(10)],
        help_text='Quantidade do ingrediente {} no lanche {}'.format(
            ingredient.name, dish.name
        )
    )

    def __str__(self):
        return 'Ingrediente {} do lanche {}'.format(self.ingredient.name, self.dish.name)
