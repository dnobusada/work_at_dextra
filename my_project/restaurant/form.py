from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from .models import Ingredient

class QuoteDishForm(forms.Form):
    ingredient_list = forms.ModelMultipleChoiceField(queryset=Ingredient.objects.all())

