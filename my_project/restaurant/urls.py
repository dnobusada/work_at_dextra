from django.urls import path
from .views import *


urlpatterns = [
    path('', index, name='index'),
    path('lanches/', DishListView.as_view(), name='lanches'),
    path('lanches/<int:pk>', DishDetailView.as_view(), name='dish-detail'),
    path('lanches/montar', dish_quote, name='dish-quote'),
]

