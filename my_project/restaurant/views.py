from .models import Dish, Ingredient, DishIngredient
from django.shortcuts import render, redirect, get_object_or_404
from django.views import generic

# Create your views here.


def index(request):
    """View functions for home page of site"""

    num_dishes = Dish.objects.all().count()
    num_ingredients = Ingredient.objects.all().count()

    context = {
        'num_dishes': num_dishes,
        'num_ingredients': num_ingredients,
    }

    return render(request, 'index.html', context=context)


class DishListView(generic.ListView):
    model = Dish


class DishDetailView(generic.DetailView):
    model = Dish


def dish_quote(request):
    pass