from django.contrib import admin
from .models import Ingredient, Dish, DishIngredient

# Register your models here.


class IngredientDetails(admin.ModelAdmin):
    list_display = ['name', 'price']


class DishDetails(admin.ModelAdmin):
    list_display = ['name']


class DishIngredientsDetails(admin.ModelAdmin):
    list_display = ['dish', 'ingredient']


admin.site.register(Ingredient, IngredientDetails)
admin.site.register(Dish, DishDetails)
admin.site.register(DishIngredient, DishIngredientsDetails)

