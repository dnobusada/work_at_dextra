from django.test import TestCase
from django.urls import resolve
from .views import *
from .models import *

# Create your tests here.


class PriceTests(TestCase):
    def test_registered_dishes_prices(self):
        self.set_db_data()
        all_dishes_query = Dish.objects.all()
        meat = Ingredient.objects.filter(name='Hamburguer de carne')[0]
        cheese = Ingredient.objects.filter(name='Queijo')[0]

        for dish in all_dishes_query:
            dish_ingredients = list(DishIngredient.objects.filter(dish_id=dish.id).all())
            calculated_value = 0

            meat_discount = dish.much_meat(dish_ingredients)
            cheese_discount = dish.much_cheese(dish_ingredients)

            for ingredient in dish_ingredients:
                calculated_value += ingredient.ingredient.price * ingredient.amount_of_ingredients

            if meat_discount > 0:
                calculated_value -= meat_discount * meat.price

            if cheese_discount > 0:
                calculated_value -= cheese_discount * cheese.price

            if dish.is_light(dish_ingredients):
                calculated_value *= 0.9

            self.assertEqual(dish.calculates_price(), calculated_value)

    def test_much_meat(self):
        self.set_db_data()
        hamburger = Ingredient.objects.filter(name='Hamburguer de carne')[0]
        dish_3_ham = Dish(name='Triplo')
        dish_3_ham.save()
        dish_3_ham = Dish.objects.filter(name='Triplo')[0]
        dish_ham_rel = DishIngredient(dish_id=dish_3_ham.id, ingredient=hamburger, amount_of_ingredients=3)
        dish_ham_rel.save()
        dish_ham_rel = DishIngredient.objects.filter(dish_id=dish_3_ham.id)
        self.assertEqual(dish_3_ham.much_meat(dish_ham_rel), 1)

    def test_much_cheese(self):
        self.set_db_data()
        cheese = Ingredient.objects.filter(name='Queijo')[0]
        dish_3_cheese = Dish(name='Triplo queijo')
        dish_3_cheese.save()
        dish_3_cheese = Dish.objects.filter(name='Triplo queijo')[0]
        dish_3x_rel = DishIngredient(dish=dish_3_cheese, ingredient=cheese, amount_of_ingredients=3)
        dish_3x_rel.save()
        dish_3x_rel = DishIngredient.objects.filter(dish_id=dish_3_cheese.id)
        self.assertEqual(dish_3_cheese.much_cheese(dish_3x_rel), 1)

    def test_is_light(self):
        self.set_db_data()
        lettuce = Ingredient.objects.filter(name='Alface')[0]
        hamburger = Ingredient.objects.filter(name='Hamburguer de carne')[0]
        cheese = Ingredient.objects.filter(name='Queijo')[0]

        dish_cheese_salad = Dish(name='X-Salada')
        dish_cheese_salad.save()
        dish_cheese_salad = Dish.objects.filter(name='X-Salada')[0]
        dish_cheese_salad_rel = DishIngredient(dish=dish_cheese_salad, ingredient=cheese)
        dish_cheese_salad_rel.save()
        dish_cheese_salad_rel = DishIngredient(dish=dish_cheese_salad, ingredient=lettuce)
        dish_cheese_salad_rel.save()
        dish_cheese_salad_rel = DishIngredient(dish=dish_cheese_salad, ingredient=hamburger)
        dish_cheese_salad_rel.save()
        dish_cheese_salad.save()

        dish_cheese_salad_rel = list(DishIngredient.objects.filter(dish_id=dish_cheese_salad.id))
        self.assertTrue(dish_cheese_salad.is_light(dish_cheese_salad_rel))

    def test_is_not_light(self):
        self.set_db_data()
        bacon = Ingredient.objects.filter(name='Bacon')[0]
        lettuce = Ingredient.objects.filter(name='Alface')[0]
        hamburger = Ingredient.objects.filter(name='Hamburguer de carne')[0]
        cheese = Ingredient.objects.filter(name='Queijo')[0]

        dish_cheese_salad_bacon = Dish(name='X-Bacon Salada')
        dish_cheese_salad_bacon.save()
        dish_cheese_salad_bacon = Dish.objects.filter(name='X-Bacon Salada')[0]
        dish_cheese_salad_bacon_rel = DishIngredient(dish=dish_cheese_salad_bacon, ingredient=cheese)
        dish_cheese_salad_bacon_rel.save()
        dish_cheese_salad_bacon_rel = DishIngredient(dish=dish_cheese_salad_bacon, ingredient=lettuce)
        dish_cheese_salad_bacon_rel.save()
        dish_cheese_salad_bacon_rel = DishIngredient(dish=dish_cheese_salad_bacon, ingredient=hamburger)
        dish_cheese_salad_bacon_rel.save()
        dish_cheese_salad_bacon_rel = DishIngredient(dish=dish_cheese_salad_bacon, ingredient=bacon)
        dish_cheese_salad_bacon_rel.save()

        dish_cheese_salad_bacon_rel = list(DishIngredient.objects.filter(dish_id=dish_cheese_salad_bacon.id))
        self.assertFalse(dish_cheese_salad_bacon.is_light(dish_cheese_salad_bacon_rel))

    def set_db_data(self):
        lettuce = Ingredient(name='Alface', price=0.4)
        lettuce.save()
        bacon = Ingredient(name='Bacon', price=2)
        bacon.save()
        hamburger = Ingredient(name='Hamburguer de carne', price=3)
        hamburger.save()
        egg = Ingredient(name='Ovo', price=0.8)
        egg.save()
        cheese = Ingredient(name='Queijo', price=1.5)
        cheese.save()

        cheese_bacon = Dish(name='X-Bacon')
        cheese_bacon.save()
        cheese_burger = Dish(name='X-Burguer')
        cheese_burger.save()
        cheese_egg = Dish(name='X-Egg')
        cheese_egg.save()
        cheese_egg_bacon = Dish(name='X-Egg Bacon')
        cheese_egg_bacon.save()

        dish_ingredients = DishIngredient(dish=cheese_bacon, ingredient=bacon)
        dish_ingredients.save()
        dish_ingredients = DishIngredient(dish=cheese_bacon, ingredient=hamburger)
        dish_ingredients.save()

        dish_ingredients = DishIngredient(dish=cheese_burger, ingredient=hamburger)
        dish_ingredients.save()
        dish_ingredients = DishIngredient(dish=cheese_burger, ingredient=cheese)
        dish_ingredients.save()

        dish_ingredients = DishIngredient(dish=cheese_egg, ingredient=hamburger)
        dish_ingredients.save()
        dish_ingredients = DishIngredient(dish=cheese_egg, ingredient=egg)
        dish_ingredients.save()
        dish_ingredients = DishIngredient(dish=cheese_egg, ingredient=cheese)
        dish_ingredients.save()

        dish_ingredients = DishIngredient(dish=cheese_bacon, ingredient=hamburger)
        dish_ingredients.save()
        dish_ingredients = DishIngredient(dish=cheese_bacon, ingredient=egg)
        dish_ingredients.save()
        dish_ingredients = DishIngredient(dish=cheese_bacon, ingredient=cheese)
        dish_ingredients.save()
        dish_ingredients = DishIngredient(dish=cheese_bacon, ingredient=bacon)
        dish_ingredients.save()


class PageTests(TestCase):
    def test_home_page(self):
        found = resolve('/lanchonete/')
        self.assertEqual(found.func, index)

    def test_make_a_dish_page(self):
        pass
